from bottle import route, run, template, static_file, get, post, delete, request,response
from sys import argv
import json
import pymysql
import os
from bottle import run, template, route, jinja2_view, request, static_file, error
import pyodbc
import pandas.io.sql as psql
import get_friends_func


connection = pymysql.connect(host='localhost',
                             user='root',
                             password='root',
                             db='hackathon_db',
                             charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor)



@route('/profile')
@jinja2_view('hello.html', template_lookup=['templates'])
def index():
    with connection.cursor() as cursor:
        sql = 'SELECT firstname,lastname from user where email like "' + request.query.get('email') + '"'
        cursor.execute(sql)
        result = cursor.fetchall()
    with connection.cursor() as cursor:
        sql = "SELECT * FROM user"
        cursor.execute(sql)
        result2 = cursor.fetchall()
    df = psql.read_sql( sql, connection)
    rs = get_friends_func.get_friends(df)
    curr_user = {
        "name": result[0]['firstname'],
        "last": result[0]['lastname'],
        "fr1":rs[0],
        "fr2": rs[1],
        "fr3": rs[2],
        "fr4": rs[3],
        "fr5": rs[4]
    }
    return {'name': curr_user["name"],'last':curr_user["last"],'fr3':curr_user['fr3'],'fr2':curr_user['fr2']}


@get("/signin")
def index():
    return template("signin.html")

@post("/new_user")
@jinja2_view('hello.html', template_lookup=['templates'])
def newuser():
    first = request.forms.get('first')
    last = request.forms.get('last')
    gen = request.forms.get('gender')
    age = '29'
    workplace = request.forms.get('workplace')
    addr = request.forms.get('address')
    num = request.forms.get('num')
    pos = request.forms.get('role')
    field = request.forms.get('field')
    food = 'Non specific'
    sport = request.forms.get('sport')
    music = request.forms.get('music')
    travel = request.forms.get('travel')
    movie = request.forms.get('movie')
    heb = '1'
    eng = '1'
    fr = '0'
    email = request.forms.get('email')
    with connection.cursor() as cursor:
        request.forms.get('')
        sql = "INSERT INTO user (firstname,lastname,gender,age,workplace,address,number,job_position,field,food,sport,music,travel,movies,hebrew,english,arabic,russian,french,spanish,italian,mandarin,portuguese,german,email) VALUES('"+first+"','"+last+"','"+gen+"',"+age+",'"+workplace+"','"+addr+"',"+num+",'"+pos+"','"+field+"','"+food+"','"+sport+"','"+music+"','"+travel+"','"+movie+"','"+heb+"','"+eng+"','"+"0','0','0','0','0','0','0','0','"+email+"')"
        cursor.execute(sql)
    with connection.cursor() as cursor:
        sql = "SELECT * FROM user"
        cursor.execute(sql)
        result2 = cursor.fetchall()
    df = psql.read_sql( sql, connection)
    rs = get_friends_func.get_friends(df)
    curr_user = {
        "name": first,
        "last": last,
        "fr1":rs[0],
        "fr2": rs[1],
        "fr3": rs[2],
        "fr4": rs[3],
        "fr5": rs[4]
    }
    return {'name': curr_user["name"],'last':curr_user["last"],'fr3':curr_user['fr3'],'fr2':curr_user['fr2']}




@get("/signup")
def index():
    return template("signup.html")

@get("/")
def index():
    return template("index.html")


@get('/js/<filename:re:.*\.js>')
def javascripts(filename):
    return static_file(filename, root='js')


@get('/css/<filename:re:.*\.css>')
def stylesheets(filename):
    return static_file(filename, root='css')


@get('/images/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
    return static_file(filename, root='images')


run(host='localhost', port=os.environ.get('PORT', 7000))
#run(host='0.0.0.0', port=argv[1])

"""This file provides categories for user generation. It includes: sports, movies, languages and travel"""

import requests
import re
from bs4 import BeautifulSoup
import pandas as pd


def get_movies():
    page = requests.get('https://www.imdb.com/chart/top')
    soup = BeautifulSoup(page.content, 'html.parser')
    movie_titles = soup.find_all(class_='titleColumn')
    movies = []
    for movie_title in movie_titles:
        movie_split = movie_title.text.split()
        print(movie_split)
        movies.append(' '.join(movie_split[1:-1]))
    return movies

movies = get_movies()
sports = ['Running', 'Basketball', 'Tennis', 'Soccer', 'Cycling', 'Crossfit',
         'Walking', 'Surfing', 'Swimming', 'Judo', 'Karate', 'Climbing', 'Yoga', 'Pilates']


languages = ['Hebrew', 'English', 'Arabic', 'Russian', 'French', 'Spanish', 'Italian', 'Mandarin', 'Portuguese',
             'German'] #, 'Japanese', 'Hindi', 'Dutch']
travel = pd.read_csv('travel.csv')
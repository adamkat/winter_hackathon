from sklearn.cluster import KMeans
import pandas as pd
import numpy as np


def get_friends(df):
    people_for_lunch = 5

    features_list = ['gender', 'age', 'Workplace', 'Job_Position', 'Field', 'Food', 'sport', 'music', 'travel',
                     'movies', 'Hebrew', 'English', 'Arabic', 'Russian', 'French', 'Spanish',
                     'Italian', 'Mandarin', 'Portuguese', 'German']

    df_clustering = df[features_list]

    l = ['sport', 'music', 'travel', 'movies']

    for i in l:
        df_clustering[i] = df_clustering[i].apply(lambda x: i + str(x))

    df_clustering = pd.get_dummies(df_clustering)

    kmeans = KMeans(n_clusters=5).fit(df_clustering[:-1])

    type_newuser = kmeans.predict([df_clustering.iloc[-1, :]])

    friends_ind = np.random.choice(np.where(kmeans.labels_ == type_newuser)[0], people_for_lunch, replace=False)

    friends = ['', '', '', '', '']

    for ind, val in enumerate(friends_ind):
        friends[ind] += df.loc[val][['firstname', 'lastname']][0] + ' ' + df.loc[val][['firstname', 'lastname']][1]

    return friends


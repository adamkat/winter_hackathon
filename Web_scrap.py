import pandas as pd
import numpy as np
import json
import requests
from datetime import datetime
from sqlalchemy import create_engine
import pymysql
import urllib, json
import urllib.request
import os

PATH = r'C:\Users\lastm\Documents\ITC\Hackathon\databases'

# Define the Start value
api_key= 'AIzaSyAw2KZIF2X3YDyAD2p3AI88lsm7HBWaHfw'
type = 'restaurant'
latitude_sarona = 32.071651
longitude_sarona = 34.786921
radius = 1500

def GoogPlac(type,latitude,longitude,radius,api_key):
  #making the url
  MyUrl = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={},{}&radius={}&type={}&key={}'.format(latitude,longitude,radius,type,api_key)
  #grabbing the JSON result
  response = urllib.request.urlopen(MyUrl)
  jsonRaw = response.read()
  jsonData = json.loads(jsonRaw)
  return jsonData

data = GoogPlac(type,latitude_sarona,longitude_sarona,radius,api_key)
# data
def get_data_frame(data):
    df = pd.DataFrame(columns=['name', 'lng', 'lat', 'rating', 'p_level'])
    for rest in data['results']:
        lat = rest['geometry']['location']['lat']
        lng = rest['geometry']['location']['lng']
        name = rest['name']
        rating = rest['rating']
        if 'price_level' in rest.keys():
            p_level = rest['price_level']
        else:
            p_level = np.nan
        df = df.append({'name': name, 'lng': lng, 'lat': lat, 'rating': rating, 'p_level': p_level}, ignore_index=True)
    return df


def table_sql_restaurant(df):
    # Connect to the database
    cnx = pymysql.connect(host='localhost',
                          user='root',
                          password='',
                          use_unicode=True,
                          charset="utf8")

    cursorObject = cnx.cursor()
    # Create a cursor object
    query_database = "CREATE DATABASE IF NOT EXISTS hackathon_db ;"

    # Execute the sqlQuery
    cursorObject.execute(query_database)

    # Use the database
    use = "USE hackathon_db"

    # run the query_use
    cursorObject.execute(use)

    # Create our Table

    query_table_products = "CREATE TABLE IF NOT EXISTS RESTAURANTS " \
                           "(ID int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (ID)," \
                           " NAME CHAR(250) , LNG FLOAT(50), LAT FLOAT(50), RATING FLOAT(50), P_LEVEL FLOAT(50));"
    # Execute the sqlQuer
    cursorObject.execute(query_table_products)


    # SQL query to insert values
    engine = create_engine("mysql://root:@localhost/hackathon_db", encoding='utf8', echo=True)
    df.to_sql('RESTAURANTS', con=engine, if_exists='append', index=1)

    cursorObject.close()
    return cnx


def table_user(cnx):
    df = pd.read_excel(os.path.join(PATH, 'User_database.xlsx'))
    name = df['Name'].apply(lambda x: x.split())
    first_name = list()
    last_name = list()
    workplace = list()
    address = list()

    name.apply(lambda x: first_name.append(x[0]))
    name.apply(lambda x: last_name.append(x[1]))

    sex = ['Male', 'Female']
    food = ['Non specific', 'Kosher', 'Gluten free']
    job = ['Marketing technologist', 'SEO analytics', 'Web Analytics specialist', 'Social Media consultant',
           'Content manager', 'UX designer', 'UI designer', 'Front-end designer', 'Back-end designer',
           'Mobile developer', 'Mobile analyst', 'Full-stack developer', 'Software developer',
           'Machine learning engineer',
           'Data scientist', 'Data engineer', 'Data analyst', 'Data researcher', 'Cloud specialist',
           'Business specialist', 'System engineer', 'Business analyst',
           'DevOps engineer', 'Product specialist', 'Product manager']
    field = ['Marketing', 'Marketing', 'Web', 'Web', 'Web', 'Designer', 'Designer', 'Developer', 'Developer',
             'Mobile', 'Mobile', 'Developer', 'Software', 'Data', 'Data', 'Data', 'Data', 'Data', 'Software',
             'Business',
             'System', 'Business', 'System', 'Product', 'Product']

    text = np.loadtxt(os.path.join(PATH, 'workplaces.txt'), dtype='str')

    company_name = list()
    road = list()
    number = list()

    for t in text:
        temp = t[0].split(',')
        company_name.append(temp[0])
        number.append(int(temp[1]))
        road.append(t[2].split(',')[0])

    company_name.extend(
        ['Dynamic yiel', 'Dudamobile', 'Quesity', 'smartCV', 'Ubimo', 'Playkers', 'Hyperactivate', 'Trax',
         'Mobiright', 'Powtoon', 'Boxee', 'kela', 'Myndlift', 'rankabove', 'getevents', 'appnext', 'clicksmob',
         'fortvision', 'cymmetria', 'moovz', 'crosswise', 'webzai', 'Simpleorder', 'myHeritage', 'rethink',
         'propel', 'pipely.io', 'joinVR', 'poptin', 'getstatus'])
    road.extend(['Eliezer Kaplan', ' Eliezer Kaplan', 'Laskov', 'Laskov', 'Ibn gvirol', 'Ibn gvirol', 'Ibn gvirol',
                 'Ibn gvirol',
                 'Carlebach', "HaArba'a", "HaArba'a", "HaArba'a", 'HaHashmonaim', 'HaHashmonaim', 'HaHashmonaim',
                 'HaHashmonaim',
                 'HaHashmonaim', 'HaHashmonaim', 'HaHashmonaim', 'HaHashmonaim', 'HaHashmonaim', 'HaHashmonaim',
                 'HaHashmonaim',
                 'Aluf Kalman Magen', 'Aluf Kalman Magen', 'Aluf Kalman Magen', 'Aluf Kalman Magen',
                 'Aluf Kalman Magen', 'Aluf Kalman Magen',
                 'Aluf Kalman Magen'])
    number.extend(
        [40, 45, 12, 16, 10, 8, 6, 4, 22, 7, 15, 57, 8, 11, 15, 17, 23, 26, 29, 31, 54, 56, 58, 20, 22, 24, 14, 16, 18,
         20])

    list_sex = list()
    list_age = list()
    list_food = list()
    list_job = list()
    list_field = list()
    list_companyname = list()
    list_road = list()
    list_number = list()

    for i in range(200):
        n = np.random.randint(0, len(company_name))
        list_sex.append(np.random.choice(sex))
        list_age.append(np.random.randint(23, 35))
        list_food.append(np.random.choice(food, p=[0.7, 0.2, 0.1]))
        m = np.random.randint(0, len(job))

        list_job.append(job[m])
        list_field.append(field[m])
        list_companyname.append(company_name[n])
        list_road.append(road[n])
        list_number.append(number[n])

    df['firstname'] = first_name
    df['lastname'] = last_name
    df['gender'] = list_sex
    df['age'] = list_age
    df['Workplace'] = list_companyname
    df['address'] = list_road
    df['number'] = list_number
    df['Job_Position'] = list_job
    df['Field'] = list_field
    df['Food'] = list_food

    df = df.drop(['Name'], axis=1)
    # df = df.drop(['Work'], axis=1)
    sport = ['Running', 'Basketball', 'Tennis', 'Soccer']
    music = ['Blues', 'Hip hop', 'Classical', 'Electronic']
    travel = ['Bangkok, Thailand', 'London, UK', 'New-York, USA', 'Buenos Aires, Argentina']
    movies = ['Inception', 'Citizen Kane', 'Pulp Fiction', 'Vertigo']
    languages = ['Hebrew', 'English', 'Arabic', 'Russian', 'French', 'Spanish', 'Italian', 'Mandarin', 'Portuguese',
                 'German']
    # Create our Table

    list_sport = list()
    list_music = list()
    list_travel = list()
    list_movies = list()

    for i in range(200):
        list_sport.append(np.random.randint(0, 4))
        list_music.append(np.random.randint(0, 4))
        list_travel.append(np.random.randint(0, 4))
        list_movies.append(np.random.randint(0, 4))

    df['sport'] = list_sport
    df['music'] = list_music
    df['travel'] = list_travel
    df['movies'] = list_movies

    languages = ['Hebrew', 'English', 'Arabic', 'Russian', 'French', 'Spanish', 'Italian', 'Mandarin', 'Portuguese',
                 'German']

    list_h = list()
    list_e = list()
    list_a = list()
    list_r = list()
    list_f = list()
    list_s = list()
    list_i = list()
    list_m = list()
    list_p = list()
    list_g = list()

    languages_list = [list_h, list_e, list_a, list_r, list_f, list_s, list_i, list_m, list_p, list_g]

    for i in range(len(df)):

        l = np.random.choice([0, 1], size=(len(languages),))

        for ind, val in enumerate(l):
            languages_list[ind].append(val)

    df['Hebrew'] = list_h
    df['English'] = list_e
    df['Arabic'] = list_a
    df['Russian'] = list_r
    df['French'] = list_f
    df['Spanish'] = list_s
    df['Italian'] = list_i
    df['Mandarin'] = list_m
    df['Portuguese'] = list_p
    df['German'] = list_g

    df['email'] = df['firstname'] + df['lastname'] + '@gmail.com'


    query_user = "CREATE TABLE IF NOT EXISTS USER " \
                           "(ID int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (ID)," \
                           "firstname CHAR(250) , lastname CHAR(250)," \
                           "gender CHAR(250), age INT(11), Workplace CHAR(250), address CHAR(250)," \
                           "number int(11), Job_Position CHAR(250), Field CHAR(250)," \
                           "Food CHAR(250), sport INT(11), music INT(11), travel INT(11)," \
                           "movies INT(11), Hebrew INT(11), English INT(11), Arabic INT(11)," \
                           "Russian INT(11), French INT(11), Spanish INT(11), Italian INT(11), Mandarin INT(11)," \
                           " Portuguese INT(11), German INT(11), email CHAR(250));"

    # Execute the sqlQuer
    cursorObject = cnx.cursor()
    cursorObject.execute(query_user)


    # SQL query to insert values
    engine = create_engine("mysql://root:@localhost/hackathon_db", encoding='utf8', echo=True)
    df.to_sql('USER', con=engine, if_exists='append', index=1)



def main():
    # Get Data from Google APi
    df = get_data_frame(data)
    df = df.drop([2,5,9,16])

    # Insert Database
    cnx = table_sql_restaurant(df)

    table_user(cnx)


if __name__ == "__main__":
  main()